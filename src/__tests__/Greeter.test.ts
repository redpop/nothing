import { Greeter } from '../index';

test('My Greeter', () => {
  expect(Greeter('Martin')).toBe('Hello Martin');
});
